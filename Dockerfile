FROM ubuntu

RUN apt update
RUN apt dist-upgrade -y
RUN apt install openjdk-17-jdk wget git -y

RUN mkdir /srv/minecraft
WORKDIR /srv/minecraft

# Download the minecraft_server.1.18.2.jar Java Edition
# RUN wget https://launcher.mojang.com/v1/objects/c8f83c5655308435b3dcf03c06d9fe8740a77469/server.jar
RUN wget https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar
RUN wget https://github.com/sladkoff/minecraft-prometheus-exporter/releases/download/v2.5.0/minecraft-prometheus-exporter-2.5.0.jar
RUN java -jar BuildTools.jar --rev latest

RUN rm BuildTools.jar

RUN echo "eula=true" >> eula.txt
RUN mkdir plugins
RUN mkdir plugins/PrometheusExporter
COPY minecraft-prometheus-exporter-2.5.0.jar plugins/minecraft-prometheus-exporter-2.5.0.jar
COPY prometheus-exporter.yaml plugins/PrometheusExporter/config.yml
COPY server.properties server.properties


ENTRYPOINT java -Xmx1024M -Xms1024M -jar spigot-1.18.2.jar nogui
